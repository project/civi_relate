
Copyright 2010 by Shawn P. Duncan
Licensed under GNU GENERAL PUBLIC LICENSE, Version 2
Sponsored by C3 Design http://www.c3design.org


CiviCRM Relate creates a CiviCRM relationship between the acting user and a CiviCRM contact created by the acting user
through the submission of a designated CiviCRM profile form.

The module has a simple configuration page which accepts via select fields the Profile to use and the relationship to create.

Installation Instructions

1. Download and unzip the archive.
2. Move the civi_relate folder to <drupal root>/sites/all/modules
3. Enable CiviCRM Relate on your Administer >> Site Building >> Modules page.
4. Select the appropriate Profile and Relationship on the configuration page found at 
Administer >> Site Configuration >> CiviCRM Relate.